var express = require('express');
var router = express.Router();
var LoginApp =  require('../LoginApp')

/* GET home page. */
router.get('/', LoginApp.web.WebIsLoggedIn.render({fail_redirect:"/login"}),function(req,res,next){
    res.render('index', { title: 'Test' });
});

module.exports = router;
