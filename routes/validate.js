var express = require('express');
var router = express.Router();
var LoginApp =  require('../LoginApp')

/* GET home page. */
router.get('/:token', LoginApp.web.WebValidateAccount.render(),function(req,res,next){
    res.render('validate', { title: 'Validate Account' });
});

module.exports = router;
