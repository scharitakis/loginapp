
const config = {
    local:{
        connectionString:'mongodb://192.168.99.100:32771/loginApp',
        connectionOptions:{
            server: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            },
            replSet: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            },
            mongos: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            }
        }
    },
    development:{
        connectionString:'mongodb://192.168.99.100:32771/loginApp',
        connectionOptions:{
            server: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            },
            replSet: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            },
            mongos: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            }
        }
    },
    production:{
        connectionString:'mongodb://192.168.99.100:32771/loginApp',
        connectionOptions:{
            server: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            },
            replSet: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            },
            mongos: {
                auto_reconnect: true,
                // socketOptions: {
                //     noDelay: true,
                //     connectTimeoutMS: 10000,
                //     socketTimeoutMS: 10000,
                //     keepAlive: true
                // },
                poolSize: 128
            }
        }
    }

}

module.exports = config[process.env.NODE_ENV || "local"];

