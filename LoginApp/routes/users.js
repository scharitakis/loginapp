var express = require('express');
var router = express.Router();

/* GET users listing. */

function UserRoutes(LoginApp) {

    router.post('/login', LoginApp.strategies.authenticateLocal(), (req, res, next)=> {
        LoginApp.api.ApiAuthenticationToken.render(req, res, next)
    })

    router.post('/logincookie', LoginApp.strategies.authenticateLocal(), (req, res, next)=> {
        LoginApp.api.ApiAuthenticationTokenCookie.render(req, res, next)
    })

    router.post('/delete', (req, res, next)=> {
        LoginApp.api.ApiDeleteUser.render(req, res, next)
    })

    router.post('/create', (req, res, next)=> {
        LoginApp.api.ApiCreateUser.render(req, res, next)
    })

    router.post('/changePass', (req, res, next)=> {
        LoginApp.api.ApiChangePass.render(req, res, next)
    })

    router.post('/validateChangePass', (req, res, next)=> {
        LoginApp.api.validateChangePass.render(req, res, next)
    })

    return router
}

module.exports = UserRoutes;
