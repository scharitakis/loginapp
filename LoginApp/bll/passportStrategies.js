var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var LinkedInStrategy   = require('passport-linkedin-oauth2').Strategy;

var LinkedInTokenStrategy  = require('passport-linkedin-token').Strategy;
var FacebookTokenStrategy  = require('passport-facebook-token');
var TwitterTokenStrategy  = require('passport-twitter-token');
var GoogleTokenStrategy  = require('passport-google-token').Strategy;

class PassportStrategies{
    constructor(options){
        this.passport = passport;
        this.db = options.db
        this.strategyTypes = {
            "local":{
                type:LocalStrategy,
                config:{usernameField: 'email', passwordField: 'passwd'}
            }
        }
        this.strategies= this.createStrategies()
    }

    createStrategies(){
        let strategyKeys = Object.keys(this.strategyTypes)
        var strategies = {}
        for(let i=0;i<strategyKeys.length;i++ ){
            var strategy = this.strategyTypes[strategyKeys[i]];
            switch(strategyKeys[i]){
                case 'local':
                    strategies[strategyKeys[i]] = new strategy.type(strategy.config,(username,password,done)=>{
                        this.localStrategy(username,password,done)})
                    break;
            }

            this.passport.use(strategies[strategyKeys[i]])
        }
        return strategies
    }

    localStrategy(username,password,done){
        console.log("username",username)
        console.log("password",password)
        //this.db.db.collection('users').insert({username:"test",password:"test"}, function(err, userData) {console.log(userData)})
        var Users = this.db.Users()
        Users.collection.findOne({username:username}, function(err, userData) {
            console.log(userData)
            if (err) {
                return done(err);
            }
            if (!userData) {
                return done(null, false);
            }
            var user = Users.create(userData)
            if (!user.validPassword(password)) {
                return done(null, false);
            }
            return done(null, user);
        });
    }

    authenticateLocal(){
        return this.passport.authenticate('local',
            {
                session: false
            })
    }


}

module.exports = PassportStrategies