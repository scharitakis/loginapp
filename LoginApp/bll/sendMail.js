const configSendGrid = require('../config/configSendGrid')
const sg = require('sendgrid')

class SendMail {
    constructor(){
        this.sg = sg.SendGrid(configSendGrid.apikey)
        this.helper = sg.mail
    }

    sendEmail(from,to,subject,email_content,clb){
        let from_email = new this.helper.Email(from)
        let to_email = new this.helper.Email(to)
        let content = new this.helper.Content("text/plain", email_content)
        let mail = new this.helper.Mail(from_email, subject, to_email, content)

        let requestBody = mail.toJSON()
        let request = this.sg.emptyRequest()
        request.method = 'POST'
        request.path = '/v3/mail/send'
        request.body = requestBody
        this.sg.API(request, function (response) {
            console.log(response.statusCode)
            console.log(response.body)
            console.log(response.headers)
            clb(response);
        })
    }



}


module.exports = SendMail;