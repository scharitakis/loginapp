var MongoDbContext = require('./MongoDBContext')
var UserSchema = require('../models/userSchema')

class LoginAppMongoContext extends MongoDbContext{
    constructor(options)
    {
        super(options);
    }

    Users(){
        return new UserSchema(this.db)
    }
    
    

}

module.exports = LoginAppMongoContext;