"use strict";

var MongoClient = require('mongodb').MongoClient
var events = require('events');


class MongoDBContext  extends events.EventEmitter {
    constructor(options){
        super();
        this.name=options.name||"MongoDBtest";
        this.connectionString = options.connectionString;
        this.connectionOptions = options.connectionOptions;
        this.mongoClient = MongoClient;
        this.connected = false;
        this.db = null
        this.on('error',e=>{options.onError || this.onError()})
        this.on('close',e=>{options.onClose || this.onClose()})
        this.on('connect',e=>{options.onConnect || this.onConnect()})
        this.on('reconnect',e=>{options.onReconnect || this.onReconnect()})

    }

    connect(){
        this.mongoClient.connect(this.connectionString,this.connectionOptions,(err,db) =>{this.connectedFunction(err,db)})
    }

    connectedFunction(err,db){
        if(!err) {
            this.connected = true
            this.db = db;
            this.db.on('error',()=>{this.emit('error')});
            this.db.on('close', ()=>{this.emit('close')});
            this.db.on('reconnect', ()=>{this.emit('reconnect')});
            this.emit('connect');
        }else{
            this.onError()
        }
    }

    close(){
        this.db.close();
    }

    onError(){
        console.log(this.name +" error")
    }

    onClose(){
        console.log(this.name +" close")
    }

    onConnect(){
        console.log(this.name +" connect")
    }
    onReconnect(){
        console.log(this.name +" reconnect")
    }

}



module.exports = MongoDBContext;