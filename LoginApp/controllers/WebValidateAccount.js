var JwtUtils = require('../utils/jwtUtils')

class WebValidateAccount extends JwtUtils{
    constructor(db){
        super();
        this.db = db;
    }

    validate(req,res,next,email){
        console.log(email)
        //TODO:check hot to get the document using the id
        this.db.Users().collection.updateOne({ username : email },{ $set: { validated : true } }, function(err, result) {
            if(err){
                console.log(err)
            }else{
                console.log(result.result)
                if(result.result.n==0){
                    res.send({res:false})
                    return;
                }
                res.send({res:true})
            }
        })
    }

    /*
    * { aud: 'admin@cnnApi.com',
     exp: 1468676661,
     iat: 1468590261,
     email: 'sce9sc@hotmail.com',
     sub: '5788e8b5cc6c0f30dcbc1257' }*/
    render(){
        var _this = this;
        return function(req,res,next){
            var token = req.params.token;
            _this.validateJWT(token,function(err,result){
                if(err){
                    console.log(err.error)
                }else{
                    _this.validate(req,res,next,result.email)
                }
            })
        }
    }

}

module.exports = WebValidateAccount;