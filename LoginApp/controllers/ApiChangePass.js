var JwtUtils = require('../utils/jwtUtils')

class ApiChangePass extends JwtUtils{
    constructor(db){
        super();
        this.db = db;
    }

    render(req,res,next){
        var  db = this.db;
        var _this = this;
        var params = req.body
        if(!params){
            res.send({error:'No params where specified'});
            res.end();
        }
        var user = params.email;
        db.Users().collection.find({username:user}).toArray(function(err,result){
            if(err){
                res.send({error:true,res:'There was an error'});
                return
            }
            if(result.length<1) {
                res.send({error:true,res:'User does not exist'});

            }else{
                console.log(result[0])
                req.user = result[0]
                var token = _this.getJwt(req,res);
                res.send({error:false,res:token});
            }
        })

    }

}

module.exports = ApiChangePass;