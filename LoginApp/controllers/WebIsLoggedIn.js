var JwtUtils = require('../utils/jwtUtils')

class WebIsLoggedIn extends JwtUtils{
    constructor(db){
        super();
        this.db = db;
    }

    render(options){
        var success_redirect = options.success_redirect||null;
        var fail_redirect = options.fail_redirect ||null;
        var _this = this;
        return function(req,res,next){
            req.fail_redirect = fail_redirect?fail_redirect:req.path
            req.success_redirect = success_redirect?success_redirect:req.path

            console.log('fail_redirect',req.fail_redirect)
            console.log('success_redirect',req.success_redirect)

            var authCookie = req.cookies['x-testApp-authorization'];
            var jwtToken = unescape(authCookie).split(' ')[1];
            if (!authCookie) {
                res.redirect(req.fail_redirect)
                return;
            }else{
                _this.checkJWT(req,res,next,jwtToken)
            }

        }
    }

}

module.exports = WebIsLoggedIn;