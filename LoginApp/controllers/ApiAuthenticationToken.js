var JwtUtils = require('../utils/jwtUtils')

class ApiAuthenticationToken extends JwtUtils{
    constructor(db){
        super();
        this.db = db;
    }

    render(req,res,next){
        console.log(req.user)
        var token = this.getJwt(req,res)
        console.log(token)
        res.send(token)
    }

}

module.exports = ApiAuthenticationToken;