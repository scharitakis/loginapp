var JwtUtils = require('../utils/jwtUtils')

class ApiCreateUser extends JwtUtils{
    constructor(db ,sendMail){
        super();
        this.db = db;
        this.mail = sendMail
    }

    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


    render(req,res,next){
        var _this = this;
        var db = this.db;
        var params = req.body
        if(!params){
            res.send({error:'No params where specified'});
            res.end();
        }

        var user = params.email;
        var password = params.passwd;

       if(!this.validateEmail(user)){
           res.send({error:true,res:'Email is not valid'});
           return
       }

        db.Users().collection.find({username:user}).toArray(function(err,result){
            if(err){
                res.send({error:true,res:'There was an error'});
                return
            }
            if(result.length<1){
                var User = db.Users().create({username:user,password:password});
                db.Users().collection.insert(User.toJSON(),function(err,res1){

                    if(err){
                        res.send({ error: 'Error creating user' });
                        return
                    }

                    req.user = res1.ops[0]
                    console.log("userCreated",req.user)

                    var token = _this.getJwt(req,res);
                    //We need to create a Jwt token to verify


                    try {
                        _this.mail.sendEmail("stavros@loginApp.com", user, "Validate your Account", "We need to validate your Acccount, http://localhost:3000/validate/"+token, function (responce) {
                            console.log("MAIL WAS SEND")
                            res.send({res: 'User created'});
                        })
                    }catch(e){
                        console.log(e)
                    }
                })
            }else{
                res.send({ res: 'User exists' });

            }
        })

    }

}

module.exports = ApiCreateUser;