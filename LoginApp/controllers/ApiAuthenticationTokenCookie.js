var JwtUtils = require('../utils/jwtUtils')

class ApiAuthenticationTokenCookie extends JwtUtils{
    constructor(db){
        super();
        this.db = db;
    }

    render(req,res,next){
        var token = this.getJwtCookie(req,res)
        res.send({success:true})
    }

}

module.exports = ApiAuthenticationTokenCookie;