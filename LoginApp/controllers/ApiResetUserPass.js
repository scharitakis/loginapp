var JwtUtils = require('../utils/jwtUtils')

class ApiResetUserPass extends JwtUtils{
    constructor(db){
        super();
        this.db = db;
    }

    render(req,res,next){
        var  db = this.db;
        var params = req.body
        if(!params){
            res.send({error:'No params where specified'});
            res.end();
        }
        var user = params.email;
        db.Users().collection.deleteOne({username:user},function(err,result){
            if(err){
                res.send({error:'Error deleting user'});
                res.end();
                return
            }
            if(result.result.n ==1){
                res.send({res:'Deleted success'});
            }else{
                res.send({res:'Failed deleting' });
            }
        })

    }

}

module.exports = ApiResetUserPass;