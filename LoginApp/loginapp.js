var passport = require('passport');
var passportStrategies = require('./bll/passportStrategies');
var sendMail = require('./bll/sendMail');

var ApiAuthenticationToken = require('./controllers/ApiAuthenticationToken');
var ApiAuthenticationTokenCookie = require('./controllers/ApiAuthenticationTokenCookie');
var ApiCreateUser = require('./controllers/ApiCreateUser');
var ApiDeleteUser = require('./controllers/ApiDeleteUser');
var ApiChangePass = require('./controllers/ApiChangePass');
var ApiValidateChangePass = require('./controllers/ApiValidateChangePass');

var WebIsLoggedIn = require('./controllers/WebIsLoggedIn');
var WebValidateAccount = require('./controllers/WebValidateAccount');

var users = require('./routes/users');

class LoginApp {
    constructor(options){
        try {
            this.strategies = new passportStrategies(options)
            this.sendMail = new sendMail();
            this.db = options.db;
            this.enabled = false;
            this.api = {
                "ApiAuthenticationToken": new ApiAuthenticationToken(this.db),
                "ApiAuthenticationTokenCookie": new ApiAuthenticationTokenCookie(this.db),
                "ApiCreateUser": new ApiCreateUser(this.db, this.sendMail),
                "ApiDeleteUser": new ApiDeleteUser(this.db),
                "ApiChangePass":new ApiChangePass(this.db),
                "ApiValidateChangePass":new ApiValidateChangePass(this.db)
            }
            this.web={
                "WebIsLoggedIn":new WebIsLoggedIn(this.db),
                "WebValidateAccount":new WebValidateAccount(this.db)
            }

            this.db.on('close', ()=>this.setEnabled(false))
            this.db.on('connect', ()=>this.setEnabled(true))
            this.db.on('reconnect', ()=>this.setEnabled(true))
            this.expressapp = null;
        }catch(e){
            console.log(e);
        }
    }

    setEnabled(state){
        console.log("LoginApp enabled:",state)
        this.enabled = state;
    }


   /* middleWare(req, res, next){
        //use it like = app.use((req, res, next)=>LoginApp.middleWare(req, res, next))
        console.log(this.enabled);
        if(this.enabled) {
            next();
        }
        else{
            res.status(500);
            res.render('error', {
                message: "Login App is disabled",
                error: {}
            });
        }
    }*/

    initExpress(app){
        this.expressapp = app;
        app.use(passport.initialize());
        app.use(function (err,req, res, next) {
            console.log(_this.enabled);
            if(_this.enabled) {
                next();
            }
            else{
                res.status(500);
                res.render('error', {
                    message: "Login App is disabled",
                    error: {}
                });
            }
        });
        app.use('/users', users(this));
        app.use(function(err, req, res, next) {
            //catch errors
            res.status(err.status || 500);
            res.send({'error':"An error occured"});
        });

    }

    /*express(app) {
        this.initExpress(app)
        var _this = this;
        return function (err,req, res, next) {
            console.log(_this.enabled);
            if(_this.enabled) {
                next();
            }
            else{
                res.status(500);
                res.render('error', {
                    message: "Login App is disabled",
                    error: {}
                });
            }
        }
    }*/



}

module.exports = LoginApp;