var bcrypt   = require('bcrypt');
const saltRounds = 10;

class Role {
    constructor(role){
        if(typeof role != 'undefined') {
            if(role._id!=undefined) {
                this.id = role._id;
                this.name = role.name;
                this.value = role.value
            }
            else{
                this.name = role.name;
                this.value = role.value
            }
        }else{
            this.name = undefined
            this.value = undefined
        }
    }

    toJSON(){
        let role = {
            name:this.name,
            value:this.value,
        }
        if(this.id){
            role.id=this.id
        }
        return role;
    }


}

module.exports = User;