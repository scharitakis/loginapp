const User = require('./user');
var ObjectID  = require('mongodb').ObjectID

class UserSchema {
    constructor(db){
        this.collectionName = 'users';
        this.db = db;
        this.collection= this.db.collection(this.collectionName);
        /*this.schema = {
            _id:{type:"ObjectId",default:""},
            username:{type:"String",default:""},
            password:{type:"String",default:""},
        }*/
    }
    
    create(user){
        return new User(user)
    }

}

module.exports = UserSchema;