var bcrypt   = require('bcrypt');
const saltRounds = 10;

class User {
    constructor(user){
        if(typeof user != 'undefined') {
            if(user._id!=undefined) {
                this.id = user._id;
                this.username = user.username;
                this.password = user.password
                this.validated = user.validated
            }
            else{
                this.username = user.username;
                this.password = this.generatePass(user.password)
                this.validated = false
            }
        }else{
            this.username = undefined
            this.password = undefined
            this.validated = false
        }
    }

    validPassword(passwd){
        var res = bcrypt.compareSync(passwd, this.password);//this.password == passwd;
        return res
    }

    generatePass(passwd){
        return bcrypt.hashSync(passwd, saltRounds);
    }

    toJSON(){
        let user = {
            username:this.username,
            password:this.password,
            validated:this.validated
        }
        if(this.id){
            user.id=this.id
        }
        return user;
    }


}

module.exports = User;