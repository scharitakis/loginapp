const Role = require('./role');
var ObjectID  = require('mongodb').ObjectID

class RolesSchema {
    constructor(db){
        this.collectionName = 'roles';
        this.db = db;
        this.collection= this.db.collection(this.collectionName);
        /*this.schema = {
         _id:{type:"ObjectId",default:""},
         name:{type:"String",default:""},
         value:{type:"Integer",default:0},
         }*/
    }

    create(role){
        return new Role(role)
    }

}

module.exports = RolesSchema;