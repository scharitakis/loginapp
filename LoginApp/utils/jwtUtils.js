var jwt = require('jsonwebtoken');
var moment = require('moment');

class JwtUtils {
    constructor(){
        this.secret = "testme";
        this.cookieName = "x-testApp-authorization";
        this.domain = '.minimob.com'
    }

    createWebtoken(req,res){
        var user = req.user;

        //var isAdmin = (user.admin!=undefined && user.admin ==true)?true:false;
        var aud = (true)?'admin@cnnApi.com':"user@cnnApi.com"
        var mytoken = {
            aud:aud,
            exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
            iat:Math.floor(new Date() / 1000),
            email:user.username,
            sub:user._id//This is the user id
        }

        var token = jwt.sign(mytoken, this.secret);
        return token
    }


    getJwtCookie(req,res){
        var token = this.createWebtoken(req,res)
        res.cookie(this.cookieName, "Bearer " + token,{maxAge: 86400000});
        return token;
    }

    getJwt(req,res){
        return this.createWebtoken(req,res)
    }

    checkJWT(req,res,next,jwtToken){
            var secret = this.secret;
            var domain = this.domain;
            var cookieName = this.cookieName;
            jwt.verify(jwtToken, secret, function (err, decoded) {
                //need to find if token has expired
                //console.log(decoded)
                if(err){
                    res.clearCookie(cookieName,{domain:domain,path:'/'});
                    var cookieParams = req.hostname.indexOf(domain)>-1?{domain:'dashboard.minimob.com',path:'/'}:{path:'/'}
                    res.clearCookie(cookieName,cookieParams);
                    res.redirect(req.fail_redirect)
                    return
                }
                if(decoded.exp > moment().unix())
                {
                    next();
                    return

                }else{
                    res.redirect(req.fail_redirect)
                    return
                }

            })
    }

    validateJWT(jwtToken,clb){
        var secret = this.secret;
        jwt.verify(jwtToken, secret, function (err, decoded) {
            if(err){
                return clb({error:"An error occured in validateJWT"},null)
            }
            if(decoded.exp > moment().unix())
            {
                return clb(null,decoded)

            }else{
                return clb({error:"An error occured in validateJWT"},null)
            }

        })
    }



}

module.exports = JwtUtils;