
const mongoConfig = require('../config/mongoConfig');
const LoginAppMongoContext = require('./dal/LoginAppMongoContext');
const loginApp = require('./loginapp');

try {
    const mongoDB = new LoginAppMongoContext(mongoConfig);
    mongoDB.connect()
    var LoginApp = new loginApp({db:mongoDB})

}
catch(e){
    console.error('this is the try catch ')
    console.error(e)
}




module.exports = LoginApp;